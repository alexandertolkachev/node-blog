(function ($) {
    var $fileInput = $("#upload");
    var $base64Input = $("#img64");
    var $imgTag = $("#img-thumbnail");

    $fileInput.on("change", function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var img = e.target.result;
                $base64Input.val(img);
                $imgTag.show().attr("src", img);
            };

            reader.readAsDataURL(this.files[0]);
        }
    })
})(jQuery);
