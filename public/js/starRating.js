(function(ko, $){
    ko.bindingHandlers.starRating = {
        init: function (element, valueAccessor) {
            $(element).addClass("starRating");
            for (var i = 0; i < 5; i++)
                $("<span>").appendTo(element);

            // Handle mouse events on the stars
            $("span", element).each(function (index) {
                $(this).hover(function () {
                        $(this).prevAll().add(this).addClass("hoverChosen")
                    },
                    function () {
                        $(this).prevAll().add(this).removeClass("hoverChosen")
                    }
                ).click(function () {
                    var observable = valueAccessor();  // Get the associated observable
                    if(observable){
                        observable(index + 1);
                    }// Write the new rating to it
                });
            });
        },
        update: function (element, valueAccessor) {
            // Give the first x stars the "chosen" class, where x <= rating
            var observable = valueAccessor();
            $("span", element).each(function (index) {
                $(this).toggleClass("chosen", index < observable());
            });
        }
    };

    function RatingVm(initialRating) {
        var self = this,
            prevUserRate;
        this.rating = ko.observable(parseInt(initialRating));
        var subscriptionBefore = this.rating.subscribe(function (prevValue) {
            prevUserRate = prevValue;
        }, this, "beforeChange");
        var subscriptionAfter = this.rating.subscribe(function (userRating) {
            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: "/post/rate",
                data: JSON.stringify({
                    rate: userRating,
                    postId: $("#post-id").val()
                })
            }).done(function () {
                document.location.reload(true);
            }).fail(function (response) {
                var message = null;
                switch (response.status) {
                    case 409:
                        message = "You have already voted for this post";
                        break;
                    case 403:
                        message = "Only registered users can vote for posts"
                        break;
                    default:
                        message = "Internal error";
                }
                subscriptionAfter.dispose();
                subscriptionBefore.dispose();
                self.rating(prevUserRate);
                self.rating = null;
                window.notify.show("danger", message, true);
            });
        });
    }

    window.initRating = function(rating){
        ko.applyBindings(new RatingVm(rating), $("#rating")[0]);
    };

})(ko, jQuery);