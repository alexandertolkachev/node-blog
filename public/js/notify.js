(function (window, $) {
    function Notify() {
        var $area = $("#notify");
        var $messageContainer = $area.find("#alert-msg");
        var fadeTimeout;
        var currentType;
        var self = this;

        this.show = function (type, message, autoHide) {
            if (currentType) {
                $area.removeClass("alert-" + currentType);
                currentType = type;
            }
            $area.addClass("alert-" + type);
            if (message) {
                $messageContainer.text(message);
            }
            $area.show();
            if (autoHide) {
                if (fadeTimeout) {
                    window.clearTimeout(fadeTimeout);
                }
                fadeTimeout = window.setTimeout(self.hide, 10000);
            }
        };

        this.changeMessage = function (newMessage) {
            $messageContainer.text(message);
        };

        this.hide = function () {
            $area.fadeOut(1000);
        }

        return this;
    }

    window.notify = new Notify();
})(window, jQuery);