const router = require("koa-router")();

router._ioRoutes = [];

router.io = function(event, handler) {
    router._ioRoutes.push({event, handler})
};

router.io.routes = function () {
    return router._ioRoutes;
};

module.exports = router;

