"use strict";

const koaIo = require("koa.io");
const app = koaIo();

app.io.routes= function (routes) {
    for (let route of routes) {
        app.io.route(route.event, route.handler);
    }
};

module.exports = app;

