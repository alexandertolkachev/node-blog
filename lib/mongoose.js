const mongoose = require("mongoose");
const config = require("../config/index");

mongoose.connect(config.get("dataBaseUri"), config.get("mongoose: options"));

module.exports = mongoose;
