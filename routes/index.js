const router = require("../lib/router");
const auth = require("../middleware/authorize");
const unauth = require("../middleware/unauthorize");

//region Account
const accountController = require("../controllers/account");
const passport = require("../middleware/authenticate");

const loginRedirectSettings = {
    successRedirect: "/",
    failureRedirect: "/account/login"
};

router.get("login", "/account/login", unauth, accountController.loginPage);
router.post("/account/login", unauth, passport.authenticate('local', loginRedirectSettings));

router.get("/account/login/facebook", unauth, passport.authenticate('facebook'));
router.get("/account/login/twitter", unauth, passport.authenticate('twitter'));
router.get("/account/login/google", unauth, passport.authenticate('google'));
router.get("/account/login/vkontakte", unauth, passport.authenticate('vkontakte'));

router.get("/account/login/facebook/callback", unauth,passport.authenticate('facebook', loginRedirectSettings));
router.get("/account/login/twitter/callback", unauth, passport.authenticate('twitter', loginRedirectSettings));
router.get("/account/login/google/callback", unauth, passport.authenticate('google', loginRedirectSettings));
router.get("/account/login/vkontakte/callback", unauth, passport.authenticate('vkontakte', loginRedirectSettings));

router.get("registration", "/account/registration", unauth, accountController.registrationPage);
router.post("/account/registration", unauth, accountController.registration);

router.get("/account/profile/:id", accountController.profilePage);
router.post("/account/profile", auth, accountController.profile);

router.get("/account/logout", auth, accountController.logout);
//endregion

//region Post
const postController = require("../controllers/post");

router.get("createPost", "/post/create", auth, postController.createPage);
router.post("/post/create", auth, postController.create);

router.get("viewPost", "/post/view/:id", postController.viewPage);
router.get("/post/list", postController.listPage);

router.get("editPost", "/post/edit/:id", auth, postController.editPage);
router.post("/post/edit", auth,postController.edit);
router.post("/post/delete", auth, postController.delete);
router.post("/post/rate", auth, postController.rate);

//endregion

//region Search
const searchController = require("../controllers/search");
router.get("/search/title", searchController.searchByTitlePage);
router.get("/search/name", searchController.searchByAuthorNamePage);
//endregion

//region Comments
const commentController = require("../controllers/comment");
router.post("/comment/add", auth, commentController.add);
router.post("/comment/remove", auth, commentController.remove);
//endregion

//region Socket routes

router.io('comment:new', function*(next, postId){
    console.log(postId);
});

//endregion

router.get("/", postController.listPage);

module.exports = router;


