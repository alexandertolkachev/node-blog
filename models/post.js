"use strict";

const mongoose = require('../lib/mongoose');
const Schema = mongoose.Schema;
const User = require("./user");
const Comment = require("./comment");
const Result = require("../util/dataBaseResult");

let schema = new Schema({
    author: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    title: {
        type: String,
        required: true
    },
    content: String,
    date: Date,
    image: String,
    rates: [{userId: Schema.Types.ObjectId, rate: Number}]
});

schema.virtual("rating").get(function () {
    const rates = this.rates;

    if (!rates.length) {
        return 0;
    }

    function selectRateValue(rateObj) {
        return rateObj.rate;
    }

    function sumNumbers(prev, next) {
        return prev + next;
    }

    return rates.map(selectRateValue).reduce(sumNumbers) / rates.length;
});

schema.statics.create = function* (title, content, image, authorId) {
    const Post = this;

    let post = new Post({title, image, content, author: authorId, date: new Date()});
    yield post.save();

    return new Result(post);
};

schema.statics.getById = function* (postId, userId) {
    if (mongoose.Types.ObjectId.isValid(postId)) {
        let post = yield this.findById(postId).populate("author");

        if (userId && !post.author.equals(userId)) {
            return new Result(null, Result.reasons.ForbiddenAction);
        }

        if (post) {
            let comments = yield Comment.getByPostId(postId);
            post.comments = comments.value;
            return new Result(post);
        }
    }
    return new Result(null, Result.reasons.MissingEntity);
};

schema.statics.getByAuthorId = function* (authorId) {
    if (authorId instanceof mongoose.Types.ObjectId) {
        //TODO: rewrite with skipping this check
        authorId = authorId.toString();
    }
    if (mongoose.Types.ObjectId.isValid(authorId )) {
        let posts = yield this.find({author: authorId});
        return new Result(posts);
    }
    return new Result(null, Result.reasons.MissingEntity);
};

schema.statics.getAll = function* () {
    let posts = yield this.find().populate("author");

    return new Result(posts);
};

schema.statics.updateById = function* (id, newPost, userId) {
    let originalPostResult = yield this.getById(id);

    if (!originalPostResult.success) {
        return originalPostResult;
    }

    if (!originalPostResult.value.author.equals(userId)) {
        return new Result(null, Result.reasons.ForbiddenAction);
    }

    newPost.date = new Date();
    let savedPost = yield originalPostResult.value.update(newPost);
    return new Result(savedPost);
};

schema.statics.removeById = function* (id, userId) {
    let postResult = yield this.getById(id);

    if (!postResult.success) {
        return postResult;
    }

    if (!postResult.value.author.equals(userId)) {
        return new Result(null, Result.reasons.ForbiddenAction);
    }

    yield postResult.value.remove();
    return new Result();
};

schema.statics.searchByTitle = function* (title) {
    let result = !title ? [] : yield this.find({title: new RegExp(title, "i")}).populate("author");
    return new Result(result);
};

schema.statics.searchByAuthorName = function* (authorName) {
    if (!authorName) {
        return new Result([]);
    }
    let authors = yield User.find({name: new RegExp(authorName, "i")});

    let posts = yield this.find({author: {$in : authors}}).populate("author");
    return new Result(posts);
};

schema.statics.rate = function* (postId, userId, rateValue) {
    let post = yield this.findById(postId);
    if (!post) {
        return new Result(null, Result.reasons.MissingEntity);
    } else if(post.isRatedBy(userId)) {
        return new Result(null, Result.reasons.AlreadyExist);
    } else {
        yield post.update({$push: {"rates": {userId: userId, rate: rateValue}}});
        return new Result(true);
    }
};

schema.methods.isRatedBy = function (userId) {
    const alreadyRated = this.rates.find(function (rateEntity) {
        return rateEntity.userId.equals(userId);
    });

    return !!alreadyRated;
};

module.exports = mongoose.model("Post", schema);