"use strict";

const mongoose = require('../lib/mongoose');
const Schema = mongoose.Schema;
const User = require("./user");
const Result = require("../util/dataBaseResult");

let schema = new Schema({
    author: {
        type: Schema.Types.ObjectId,
        ref: "User",
        require: true
    },
    post: {
        type: Schema.Types.ObjectId,
        require: true
    },
    content: String,
    date: Date
});

schema.statics.create = function* (postId, authorId, content){
    const Comment = this;
    if (!authorId) {
        return new Result(null, Result.reasons.ForbiddenAction);
    }

    let comment = new Comment({author: authorId, post: postId, content:content, date: new Date()});
    yield comment.save();

    return new Result(comment);
};

schema.statics.getByPostId = function* (postId){
    let comments = yield this.find({post:postId}).populate("author");
    return new Result(comments);
};

schema.statics.delete = function* (commentId, initiator) {
    let comment = yield this.findOne({ _id: commentId });
    if (comment.author.equals(initiator)) {
        yield comment.remove();
        return new Result();
    } else {
        return new Result(null, Result.reasons.ForbiddenAction);
    }
};

module.exports = mongoose.model("Comment", schema);