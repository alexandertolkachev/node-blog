"use strict";

const mongoose = require('../lib/mongoose');
const Result = require("../util/dataBaseResult");

let schema = new mongoose.Schema({
    email: {
        type: String,
    },
    oauthId: {
        type: Number,
    },
    name: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    }
});

schema.statics.create = function* (email, name, password) {
    const User = this;

    let existedUser = yield User.findOne({email: email});
    if (!existedUser) {
        let newUser = new User({email, name, password});
        yield newUser.save();
        return new Result(newUser);
    }
    return new Result(null, Result.reasons.AlreadyExist);
};

schema.statics.verifyOAuth = function* (oauthId, name) {
    const User = this;

    let existedUser = yield User.findOne({oauthId: oauthId});
    if (!existedUser) {
        let newUser = new User({oauthId: oauthId, name: name});
        yield newUser.save();
        existedUser = newUser;
    }
    return new Result(existedUser);
};

schema.statics.verifyLocal = function* (email, password) {
    const User = this;

    let existedUser = yield User.findOne({email: email});
    let result = new Result();
    if (!existedUser) {
        result.reason = Result.reasons.MissingEntity;
    } else if (existedUser.password === password) {
        result.value = existedUser;
    } else {
        result.reason = Result.reasons.DataMismatch;
    }
    return result;
};

schema.statics.getById = function* (userId, initiator) {
    if (mongoose.Types.ObjectId.isValid(userId)) {
        let user = yield this.findById(userId);

        if (initiator && !user._id.equals(initiator)) {
            return new Result(null, Result.reasons.ForbiddenAction);
        }

        return new Result(user);
    }
    return new Result(null, Result.reasons.MissingEntity);
};

schema.statics.updateById = function* (userId, newUser, initiator){
    let userResult = yield this.getById(userId, initiator);
    if (!userResult.success) {
        return userResult;
    }
    delete newUser._id;

    yield userResult.value.update(newUser);
    return new Result(userResult.value);

};

module.exports = mongoose.model("User", schema);


