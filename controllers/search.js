"use strict";

const Post = require("../models/post");
const HttpError = require("../util/httpError");

module.exports = {
    searchByTitlePage: function* () {
        let searchResults = yield Post.searchByTitle(this.request.query.query);
        yield this.render('/post/list.jade', {posts: searchResults.value});
    },
    searchByAuthorNamePage: function* () {
        let searchResults = yield Post.searchByAuthorName(this.request.query.query);
        yield this.render('/post/list.jade', {posts: searchResults.value});
    }
};