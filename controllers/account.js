"use strict";

const User = require("../models/user");
const Post = require("../models/post");

const registrationTitle = "Registration of new user";
const loginTitle = "Log in";

module.exports = {
    loginPage: function* () {
        yield this.render('/account/login.jade', {title: loginTitle});
    },
    registrationPage: function* () {
        yield this.render('/account/registration.jade', {title: registrationTitle});
    },
    registration: function* () {
        let email = this.request.body.email;
        let name = this.request.body.name;
        let password = this.request.body.password;
        let repassword = this.request.body.repassword;
        let error = null;

        if (password === repassword) {
            let userResult = yield User.create(email, name, password);
            if (userResult.success) {
                this.redirect("/account/login");
            } else {
                error = "User with such email already exists!";
            }
        } else {
            error = "Passwords should be equals!";
        }

        yield this.render('/account/registration.jade', {title: registrationTitle, error: error});
    },
    logout: function* () {
        this.logout();
        this.redirect("/");
    },
    profilePage: function* () {
        let userResult = yield User.getById(this.params.id);
        let user = this.processDbResult(userResult);

        let postResult =yield Post.getByAuthorId(user._id);
        let posts = this.processDbResult(postResult);

        yield this.render('/account/profile.jade',{profile: user, posts});
    },
    profile: function* () {
        let updatedUser = this.request.body;
        let userResult = yield User.updateById(updatedUser.id, updatedUser, this.session.passport.user);

        let user = this.processDbResult(userResult);
        this.redirect("/account/profile/"+ user._id);
    }
};







