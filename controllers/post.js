"use strict";

const Post = require("../models/post");

module.exports = {
    createPage: function* () {
        yield this.render('/post/create.jade', {title: "Create new post"});
    },
    create: function* () {
        const req = this.request.body;
        let newPostResult = yield Post.create(req.title, req.content, req.image, this.session.passport.user);
        let newPost = this.processDbResult(newPostResult);
        this.redirect("/post/view/" + newPost._id);
    },
    listPage: function* () {
        let postsResult = yield Post.getAll();
        let posts = this.processDbResult(postsResult);
        yield this.render("/post/list.jade", {posts});
    },
    viewPage: function* () {
        let postResult = yield Post.getById(this.params.id);
        let post = this.processDbResult(postResult);
        yield this.render("/post/view.jade", post);
    },
    editPage: function* () {
        let postResult = yield Post.getById(this.params.id, this.session.passport.user);
        let post = this.processDbResult(postResult);
        yield this.render("/post/edit.jade", post);
    },
    edit: function* () {
        let postResult = yield Post.updateById(
            this.request.body.id,
            this.request.body,
            this.session.passport.user);
        this.processDbResult(postResult);
        this.redirect("/post/view/" + this.request.body.id);
    },
    delete: function* () {
        let postResult = yield Post.removeById(this.request.body.id, this.session.passport.user);
        this.processDbResult(postResult);
        this.redirect("/post/list");
    },
    rate: function* () {
        let postResult = yield Post.rate(this.request.body.postId,
            this.session.passport.user,
            this.request.body.rate);
        this.processDbResult(postResult);
        this.redirect(this.request.header.referer);
    }
};