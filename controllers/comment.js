"use strict";

const Comment = require("../models/comment");

module.exports = {
    add: function* () {
        let commentResult = yield Comment.create(
            this.request.body.postId,
            this.session.passport.user,
            this.request.body.content);

        let comment = this.processDbResult(commentResult);
        this.app.io.emit("comment:new");
        this.redirect(this.request.header.referer);
    },
    remove: function* () {
        let result = yield Comment.delete(this.request.body.commentId, this.session.passport.user);
        this.processDbResult(result);
        this.app.io.emit("comment:removed");
        this.redirect(this.request.header.referer);
    }
};