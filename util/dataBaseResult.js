"use strict";

const reasons = {
    Ok: 0,
    MissingEntity : 1,
    ForbiddenAction: 2,
    AlreadyExist: 3,
    DataMismatch: 4
};

const DataBaseResult = function (value, reason, message) {
    this.value = value || null;
    this.reason = reason;
    this.message = message;

    Object.defineProperty(this, "success", {
        get: function() {
            return typeof this.reason === "undefined";
        }
    });
};

DataBaseResult.reasons = reasons;

module.exports = DataBaseResult;
