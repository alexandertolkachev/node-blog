##** Installation and running**

```
$ npm start
```
This command will run npm install and run application.
To configure application you should set 2 parameters:

 1. **dataBaseUri** - path to mongo db

 2. **port** - port for running application

Both parameters are included in *./config/config.json* or can be set as *environment variables*.

To use Node JS blog you should use **node.js** version 4 or higher.