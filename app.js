const app = require("./lib/koa");
const mongoose = require("./lib/mongoose");
const config = require("./config");
const session = require("koa-session-store");
const mongooseStorageSession = require("koa-session-mongoose");
const bodyParser = require('koa-bodyparser');
const view = require("koa-views");
const router = require("./routes");
const path = require("path");
const errorHandler = require("./middleware/errorHandler");
const passport = require("./middleware/authenticate");
const socket = require("./middleware/socket");
const dataBaseResultHandler = require("./middleware/dataBaseResultHandler");
const serve = require('koa-static-folder');
const renderingUtils = require("./middleware/renderingUtils");

app.keys = ["secret keys"];
app.use(session({store: mongooseStorageSession.create({connection: mongoose})}));
app.use(serve("./public"));
app.use(bodyParser({formLimit: "5mb"}));
app.use(view(path.join(__dirname, 'views'), {'default': 'jade'}));

app.use(errorHandler());
app.use(dataBaseResultHandler());

app.use(passport.initialize());
app.use(passport.session());

app.use(renderingUtils());

app.use(router.routes());

app.io.use(socket);
app.io.routes(router.io.routes());

app.listen(config.get("PORT"));
