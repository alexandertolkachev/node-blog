"use strict";

const co = require("co");
const passport = require("koa-passport");
const FacebookStrategy = require("passport-facebook").Strategy;
const TwitterStrategy = require("passport-twitter").Strategy;
const GoogleStrategy = require("passport-google-oauth2").Strategy;
const VkontakteStrategy = require("passport-vkontakte").Strategy;
const LocalStrategy = require("passport-local").Strategy;

const User = require('../models/user.js');
const config = require('../config/index');

const Result = require("../util/dataBaseResult");

function processOAuthResponse(accessToken, refreshToken, profile, done){
    co(function*(){
        return yield User.verifyOAuth(profile.id, profile.displayName);
    }).then(function(userResult){
        if (userResult.success) {
            done(null, userResult.value);
        } else {
            done(userResult.reason, false);
        }
    });
}

function processLocalAuth(name, password, done) {
    co(function*() {
        return yield User.verifyLocal(name, password);
    }).then(function(userResult){
        if (userResult.success) {
            done(null, userResult.value);
        } else {
            done(null, false, userResult.reason);
        }
    });
}

passport.use(new LocalStrategy({usernameField: "email"}, processLocalAuth));
passport.use(new VkontakteStrategy(config.get("oauth:vkontakte"), processOAuthResponse));
passport.use(new FacebookStrategy(config.get("oauth:facebook"), processOAuthResponse));
passport.use(new TwitterStrategy(config.get("oauth:twitter"), processOAuthResponse));
passport.use(new GoogleStrategy(config.get("oauth:google"), processOAuthResponse));

passport.serializeUser(function(user, done) {
    done(null, user._id);
});
passport.deserializeUser(function(id, done) {
    co(function* () {
        return yield User.findById(id);
    }).then(function (user) {
        if (user) {
            done(null, user);
        }
    }).catch(function (err) {
        done(err, null);
    });
});

module.exports = passport;
