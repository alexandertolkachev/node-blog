const HttpError = require("../util/httpError");
const util = require("util");

module.exports = function() {
    "use strict";

    return function* (next) {
        "use strict";
        try {
            yield next;
            if (this.status === 404) {
                throw new HttpError(404);
            }
        } catch(err) {
            console.trace(err.stack);
            this.status = err.status || 500;
            yield this.render("/shared/error.jade", {err});
        }
    }
};
