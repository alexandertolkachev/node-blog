"use strict";

const HttpError = require("../util/httpError");

module.exports = function* (next) {
    if (this.req.isAuthenticated()) {
        yield next;
    } else {
        throw new HttpError(401, "You should be logged in to perform this action");
    }
};

