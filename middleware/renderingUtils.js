"use strict";

const moment = require("moment")

module.exports = function () {
    return function* (next) {
        this.state.utils = {
            moment
        };
        if (this.req.isAuthenticated()) {
            this.state.user = {
                name: this.passport.user.name,
                _id: this.passport.user._id
            }
        }

        yield next;
    }
};