"use strict";

const HttpError = require("../util/httpError");
const r = require("../util/dataBaseResult");

module.exports = function () {
    return function* (next) {
        this.processDbResult = function (result) {
            if (!(result instanceof r)) {
                throw new Error("Variable result should be instance of DataBaseResult");
            }
            if (result.success) {
                return result.value;
            }
            switch (result.reason) {
                case r.reasons.ForbiddenAction:
                    throw new HttpError(403, result.message || "Access denied.");
                case r.reasons.MissingEntity:
                    throw new HttpError(404, result.message || "No found.");
                case r.reasons.AlreadyExist:
                    throw new HttpError(409, result.message || "This entity already exists.");
                default:
                    throw new HttpError(500, result.message || "Internal server error.");
            }
        };
        yield next;
    };
}