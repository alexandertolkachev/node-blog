"use strict";

const HttpError = require("../util/httpError");

module.exports = function* (next) {
    if (!this.req.isAuthenticated()) {
        yield next;
    } else {
        this.redirect("/");
    }
};
